import netCDF4
import numpy
import requests
from datetime import datetime, time, timedelta
from itertools import starmap
from math import floor, ceil
from pathlib import Path
from subprocess import run, PIPE, STDOUT

from .. import Provider
from ...targets.sflux import SfluxAir


class DCPC(Provider, SfluxAir):


    class Dataset(Provider.Dataset):

        url_params = dict
        time_ranges_hours = tuple


    datasets = dict(
        arpege_europe = Dataset(
            # https://donneespubliques.meteofrance.fr
            #                       /?fond=produit&id_produit=130&id_rubrique=51
            extent_range = dict(
                lon = (-32., 42.),
                lat = (20., 72.),
            ),
            spatial_resolution = .1,

            period = timedelta(hours=48),
            time_resolution = timedelta(hours=1),
            daily_updates = ( time(0), ),

            url_params = dict(
                model = 'ARPEGE',
                grid = '0.1',
                package = 'SP1',
            ),

            time_ranges_hours =
                            ( (0, 12), (13, 24), (25, 36), (37, 48), (49, 60) ),
            # time_ranges_hours = ( (0, 12), (13, 24), (25, 36), (37, 48),
            #     (49, 60), (61, 72), (73, 84), (85, 96), (97, 102), (103, 114) ),
        ),
        arpege_global = Dataset(
            # https://donneespubliques.meteofrance.fr
            #                       /?fond=produit&id_produit=130&id_rubrique=51            extent_range = DCPCDataset._global_range,
            extent_range = Dataset._global_range,
            spatial_resolution = .5,

            period = timedelta(hours=48),
            time_resolution = timedelta(hours=3),
            daily_updates = ( time(0), ),

            url_params = dict(
                model = 'ARPEGE',
                grid = '0.5',
                package = 'SP1',
            ),

            time_ranges_hours = ( (0, 24), (27, 48), (51, 72) ),
                        #  ( (0, 24), (27, 48), (51, 72), (75, 103), (105, 114) ),
        ),
    )

    _base_url = 'http://dcpc-nwp.meteo.fr/services/PS_GetCache_DCPCPreviNum'
    _base_url_params = dict(
        token = '__5yLVTdr-sGeHoPitnFc7TZ6MhBcJxuSsoZp6y0leVHU__',
        format = 'grib2',
    )

    def __init__(self, *args, netcdf_cache_path=None, **kwargs):
        super().__init__(*args, **kwargs)

        self.__netcdf_cache_path = netcdf_cache_path

    def _url_params(self, date_time):

        ref_time_str = date_time.isoformat(timespec='seconds')
        url_params_ = lambda begin_hour, end_hour: \
            dict(
                **self._base_url_params,
                **self.dataset.url_params,

                referencetime = f'{ref_time_str}Z',
                time = f'{begin_hour:02}H{end_hour}H',
            )

        return tuple( starmap(url_params_, self.dataset.time_ranges_hours) )

    @Provider.stateful_method
    def _get_netcdf(self, delete_temp_files=True, session=None):
        self.logger.debug('_get_netcdf')

        url_params = self._url_params(self.date_time)
        self.logger.debug('url_params: %s', url_params)

        grib_download_session = session or self._requests_retry_session()
        grib_parts_filepaths, temp_filepaths = list(), list()

        def register_temp_file_(name, file_list=None, dirpath=self.work_path):
            temp_filepath = dirpath / name
            self.logger.debug('new temp file: %s', temp_filepath)

            if file_list is not None:
                file_list.append(temp_filepath)

            return temp_filepath

        try:
            for part_no, params in enumerate(url_params):
                grib_part_filepath = register_temp_file_(f'part_{part_no}.grib',
                                                           grib_parts_filepaths)

                with open(grib_part_filepath, mode='w+b') as grib_part_file:
                    self._download_grib_files(params, grib_part_file,
                                                  session=grib_download_session)

            grib_combined_filepath = register_temp_file_('combined.grib',
                                                                 temp_filepaths)
            self._copy_gribs(grib_parts_filepaths, grib_combined_filepath,
                                     where_clause='shortName=msl/10u/10v/2t/2r')

            try:
                cache_file_path = Path(self.__netcdf_cache_path, 'combined.nc')
                local_file_path = self.work_path / 'combined.nc'
                local_file_path.rename(cache_file_path)
                local_file_path.symlink_to(cache_file_path)
            except TypeError:
                pass

            netcdf_combined_filepath = register_temp_file_('combined.nc',
                                                                 temp_filepaths)
            self._convert_grib_into_netcdf(grib_combined_filepath,
                                                       netcdf_combined_filepath)

            nc_dataset = netCDF4.Dataset(netcdf_combined_filepath)
            self.logger.debug('netCDF dataset: %s', nc_dataset)
            return nc_dataset

        finally:
            if delete_temp_files:
                for tmp_filepath in grib_parts_filepaths + temp_filepaths:
                    self.logger.debug('unlink: %s', tmp_filepath)
                    tmp_filepath.unlink()

    @staticmethod
    def _requests_retry_session(retries=10, backoff_factor=1,
                                        status_forcelist=(429, ), session=None):

        retry = requests.packages.urllib3.util.retry.Retry(
            total = retries,
            read = retries,
            connect = retries,
            backoff_factor = backoff_factor,
            status_forcelist = status_forcelist,
        )

        adapter = requests.adapters.HTTPAdapter(max_retries=retry)

        session = session or requests.Session()
        session.mount('http://', adapter)
        session.mount('https://', adapter)

        return session

    def _download_grib_files(self, params, output_file, chunk_size=4096,
                                                                  session=None):

        session_ = session or requests.Session()

        response = session_.get(self._base_url, params=params)
        self.logger.debug('status_code: %s\nurl: %s\nheaders: %s',
                            response.status_code, response.url, response.headers)

        response.raise_for_status()

        for chunk in response.iter_content(chunk_size=chunk_size):
            output_file.write(chunk)

        return response, session

    _default_grib_copy_bin_path = '/usr/bin/grib_copy'

    def _copy_gribs(self, grib_files, grib_output_file, where_clause='',
                                                       grib_copy_bin_path=None):

        grib_filenames = tuple( file.name for file in grib_files )
        where_clause_tuple = ('-w', where_clause) if where_clause else ()

        self.logger.debug('grib filenames: %s', grib_filenames)

        grib_copy_args = (
            grib_copy_bin_path or self._default_grib_copy_bin_path, '-f',
            *where_clause_tuple,
            *grib_files,
            grib_output_file,
        )
        self.logger.debug('args: %s', grib_copy_args)

        execution = run(grib_copy_args, stdout=PIPE, stderr=STDOUT,
                        encoding='utf8', errors='replace')
        self.logger.debug('stdout: \n%s', execution.stdout)

        return execution

    _default_grib_to_netcdf_bin_path = '/usr/bin/grib_to_netcdf'

    # netcdf data type: NC_BYTE, NC_SHORT, NC_INT, NC_FLOAT, NC_DOUBLE
    def _convert_grib_into_netcdf(self, grib_file, netcdf_file,
                     netcdf_data_type='NC_FLOAT', grib_to_netcdf_bin_path=None):

        grib_to_netcdf_args = (
            grib_to_netcdf_bin_path or self._default_grib_to_netcdf_bin_path,
            grib_file,
            '-D', netcdf_data_type,
            '-o', netcdf_file,
        )
        self.logger.debug('args: %s', grib_to_netcdf_args)

        execution = run(grib_to_netcdf_args, stdout=PIPE, stderr=STDOUT,
                        encoding='utf8', errors='replace')
        self.logger.debug('stdout: \n%s', execution.stdout)

        return execution

    @Provider.stateful_method
    def _prepare(self):
        (lon_min, lon_max), (lat_min, lat_max) = ( self.extent[axis]
                                                    for axis in ('lon', 'lat') )

        lon_var, lat_var, time_var = ( self._get_netcdf().variables[ref][:]
                                  for ref in ('longitude', 'latitude', 'time') )

        spatial_resolution = self.dataset.spatial_resolution
        epsilon = spatial_resolution * .1

        clamp = lambda mode, value, unit: mode(value / unit) * unit
        clamp_floor = lambda coord: clamp(floor, coord, spatial_resolution)
        clamp_ceil = lambda coord: clamp(ceil, coord, spatial_resolution)

        lon_min_cf, lat_min_cf = map( clamp_floor, (lon_min, lat_min) )
        lon_max_cc, lat_max_cc = map( clamp_ceil, (lon_max, lat_max) )
        self.logger.debug('lon_min_cf=%s lon_max_cc=%s lat_min_cf=%s lat_max_cc=%s',
                                 lon_min_cf, lon_max_cc, lat_min_cf, lat_max_cc)

        lon_bool = numpy.logical_and(lon_min_cf-epsilon <= lon_var,
                                                  lon_var <= lon_max_cc+epsilon)
        lat_bool = numpy.logical_and(lat_min_cf-epsilon <= lat_var,
                                                  lat_var <= lat_max_cc+epsilon)
        self.logger.debug('lon_bool=%s\nlat_bool=%s', lon_bool, lat_bool)

        lon_set, lat_set = lon_var[lon_bool], numpy.flipud(lat_var[lat_bool])
        self.logger.debug('lon_set=%s\nlat_set=%s', lon_set, lat_set)

        self._lon_meshgrid, self._lat_meshgrid = numpy.meshgrid(lon_set,
                                                                        lat_set)

        time_set = time_var / 24.
        self._time_relative = time_set - time_set[0]
        self.logger.debug('time_relative_set=%s', self._time_relative)

        self._var_range = (..., lat_bool, lon_bool)
        # self.logger.debug('var_range:', self.var_range)

    def _sflux_date_time(self):
        return self.date_time

    def _sflux_work_path(self):
        return self.work_path

    def _sflux_dimensional_data(self):
        self._prepare()
        return dict(
            t = self._time_relative,
            long = self._lon_meshgrid,
            latt = self._lat_meshgrid,
        )

    def _sflux_map_data(self, **variable_map):
        netcdf = self._get_netcdf()
        netcdf_variable_data = lambda name: \
                          numpy.flip(netcdf.variables[name][self._var_range], 1)
        return { sflux_name: netcdf_variable_data(netcdf_name)
                           for sflux_name, netcdf_name in variable_map.items() }

    @Provider.cached_method
    def _sflux_wind_data(self):
        return self._sflux_map_data(u='u10', v='v10')

    @Provider.cached_method
    def _sflux_air_data(self):
        return dict(self._sflux_wind_data(),
            **self._sflux_map_data(p='msl', rh='r2', tmp='t2m')
        )

    def close(self):
        self._get_netcdf().close()


def download(dataset, target, kind, year, month, day, hour, period, \
                                               x_min, x_max, y_min, y_max, dir):
    from ...core import enable_logging
    enable_logging()

    dcpc = DCPC(dataset,
        date_time = datetime( *map(int, (year, month, day, hour) ) ),
        period = timedelta( hours=int(period) ),
        extent = dict(
            lon = ( float(x_min), float(x_max) ),
            lat = ( float(y_min), float(y_max) ),
        ),
        work_path = dir,
    )

    if target == 'sflux':
        kind_list = kind.split(',')
        sflux_kwargs = dict(
            dir_path = dir,
        )

        if 'air' in kind_list:
            dcpc.create_sflux_air(**sflux_kwargs)
        # if 'rad' in kind_list:
        #     martec.create_sflux_rad(**sflux_kwargs)
        if 'input' in kind_list:
            dcpc.create_inputs_txt(**sflux_kwargs)
        if 'wind' in kind_list:
            dcpc.create_sflux_wind(**sflux_kwargs)

    dcpc.close()


if __name__ == '__main__':
    from sys import argv
    download(*argv[1:])
