from collections import namedtuple, Mapping, Sequence
from datetime import time, timedelta
from functools import wraps
from inspect import getmembers
from pathlib import Path

from ..core import Logging


class Provider(Logging):


    class Dataset:

        extent_range = Mapping
        spatial_resolution = (Mapping, float, None)
        grid_size = (Mapping, None)
        period = timedelta
        daily_updates = Sequence
        time_resolution = timedelta

        _global_range = dict(
            lon = (-180., 180.),
            lat = (-90., 90.),
        )

        _every_twelve_hours = ( time(0), time(12) )
        _every_six_hours = ( time(0), time(6), time(12), time(18) )

        def _every_(interval, *, offset=timedelta(0) ):
            pass

        def _grid_size(self):
            if self.grid_size and not self.spatial_resolution:
                (min_x, max_x), (min_y, max_y) = ( self.extent_range[axis]
                                                    for axis in ('lon', 'lat') )

                self.spatial_resolution = dict(
                    horizontal = (max_x - min_x) / self.grid_size['horizontal'],
                    vertical = (max_y - min_y) / self.grid_size['vertical'],
                )

        def __init__(self, **kwargs):
            self.__data_member_names = list()

            for name, kind in getmembers(self):

                if name.startswith('_'):
                    continue

                value = kwargs.get(name)
                kind_options = kind if isinstance(kind, Sequence) else (kind, )

                if (None in kind_options and value is None) \
                                                     or isinstance(value, kind):
                    setattr(self, name, value)
                else:
                    raise TypeError(
                        f"{type(self).__qualname__}.{name} expected "
                        + ' or '.join( kind.__name__ for kind in kind_options )
                        + f", not {value}!"
                    )

                self.__data_member_names.append(name)

            for name in self.__data_member_names:
                try:
                    getattr(self, f'_{name}')()
                except (AttributeError, TypeError):
                    pass

        def __str__(self):
            return str( vars(self) )


    # dataset_cls = namedtuple('ServiceDataset', 'extent_range spatial_resolution'
    #                                     ' period daily_updates time_resolution')

    # @classmethod
    # def dataset_also_has_(cls, *fields, prefix=''):
    #     flat_fields = tuple( chain.from_iterable(
    #                        nested_fields.split() for nested_fields in fields ) )

    # # def dataset_also_has_(cls, fields, name=None):
    # #     try:
    # #         fields = fields.split()
    # #     except AttributeError:
    # #         pass

    #     return namedtuple(f'{prefix}{cls.dataset_cls.__name__}',
    #                                   (*cls.dataset_cls._fields, *flat_fields) )

    # @classmethod
    # def Dataset(cls, *, grid_size=None, **kwargs):
    #     if grid_size:
    #         (min_x, max_x), (min_y, max_y) = ( kwargs['extent_range']
    #                                                 for axis in ('lon', 'lat') )
    #         kwargs.update(
    #             spatial_resolution = dict(
    #                 horizontal = (max_x - min_x) / grid_size['horizontal'],
    #                 vertical = (max_y - min_y) / grid_size['vertical'],
    #             )
    #         )

    #     new_dataset = cls.dataset_cls(**kwargs)
    #     cls.logger.debug("%s: new dataset with %s %s",
    #                               cls.dataset_cls.__name__, kwargs, new_dataset)
    #     return new_dataset

    datasets = dict()

    def __init__(self, dataset_name, *, date_time, period, extent,
                                       work_path='.', use_cache=True, **kwargs):
        super().__init__(**kwargs)

        self.dataset = self.datasets[dataset_name]
        self.date_time = self._check_date_time(date_time)
        self.period = self._check_period(period)
        self.extent = self._check_extent(extent)
        self.work_path = self._check_work_path(work_path)

        self.__use_cache = use_cache
        self.__managed_objects = list()

    # def __init_update_arg(self, update):

    #     dataset_updates = self.dataset['daily_updates']

    #     if not update:
    #         self.update = dataset_updates[0]

    #     elif update in dataset_updates:
    #         self.update = update

    #     else:
    #         raise ValueError(
    #                   '{} update is not in {}'.format(update, dataset_updates) )

    # def __init_extent_arg(self, extent):

    #     self.extent = self.dataset['extent_range'].copy()

    #     if extent:
    #         for axis, (min_, max_) in self.extent.items():
    #             within_limits = lambda value: min_ <= value <= max_

    #             if not all( map(within_limits, extent[axis]) ):
    #                 raise ValueError('%s range limits are [%f; %f]', axis,
    #                                                                  min_, max_)

    #         self.extent.update(extent)

    # def __init_period_arg(self, period):

    #     dataset_period = self.dataset['period']

    #     if not period:
    #         self.period = dataset_period
    #         return

    #     if not isinstance(period, timedelta):
    #         raise TypeError('period argument must be a timedelta instance'
    #                                                     f", not {type(period)}")

    #     if timedelta(hours=1) <= period <= dataset_period:
    #         self.period = period

    #     else:
    #         raise ValueError(
    #                'period must be >= 1 hour and <= {}'.format(dataset_period) )

    def _check_date_time(self, date_time):
        return date_time

    def _check_period(self, period):
        return period

    def _check_extent(self, extent):
        return extent

    def _check_work_path(self, path_like):
        return Path(path_like)

    def _manage(self, object):
        self.__managed_objects.append(object)
        return object

    def close(self):
        for object in reversed(self.__managed_objects):
            object.close()

    @classmethod
    def cached_method(cls, func, stateless=True):

        @wraps(func)
        def wrapper(self, *args, _no_cache=False, **kwargs):
            call_func = lambda: func(self, *args, **kwargs)

            if stateless and ( _no_cache or not self.__use_cache ):
                return call_func()

            attr_name = f'_cached_{func.__qualname__}'

            try:
                return getattr(self, attr_name)
            except AttributeError:
                pass

            return self.__dict__.setdefault(attr_name, call_func() )

        return wrapper

    @classmethod
    def stateful_method(cls, func):
        return cls.cached_method(func, stateless=False)

    @staticmethod
    def _convert_milibar_to_pascal(data):
        '''pa = mbar * 100'''
        return data * 100

    @staticmethod
    def _convert_celsius_to_kelvin(data):
        '''K = C + 273.15'''
        return data + 273.15
