from collections import OrderedDict
from datetime import datetime as dt_datetime, date as dt_date, \
                                      time as dt_time, timedelta as dt_timedelta
from itertools import chain
from logging import getLogger, basicConfig
from netCDF4 import Dataset
from pathlib import Path
from sys import argv

from .. import Provider
from ...targets.schism.open_bnd import OpenBoundary, OpenBoundaryPhysics2D, \
                             OpenBoundaryPhysics3D, OpenBoundaryBiogeochemical3D

DEBUG = True
logger = getLogger(__name__)


class _CMEMSCommon(Provider):


    class Dataset(Provider.Dataset):

        service_id = str
        product_id = str
        depth_levels = int


    _global_range = dict(
        lon = (-180., 180.),
        lat = (-90., 90.),
    )
    _ibi_range = dict(
        lon = (-19., 5.),
        lat = (26., 56.),
    )

    _global_spatial_resolution = 1 / 12,
    _ibi_spatial_resolution = 1 / 36,


class _CMEMSPhysicsCommon(_CMEMSCommon):

    _global_af_service_id = 'GLOBAL_ANALYSIS_FORECAST_PHY_001_024-TDS'
    _ibi_af_service_id = 'IBI_ANALYSISFORECAST_PHY_005_001-TDS'


class CMEMSPhysics3D(_CMEMSPhysicsCommon, OpenBoundaryPhysics3D):

    datasets = dict(
        global_af = _CMEMSCommon.Dataset(
            extent_range = _CMEMSPhysicsCommon._global_range,
            spatial_resolution = _CMEMSPhysicsCommon._global_spatial_resolution,
            depth_levels = 43,

            time_resolution = dt_timedelta(days=1),

            service_id = _CMEMSPhysicsCommon._global_af_service_id,
            product_id = 'global-analysis-forecast-phy-001-024',
        ),
        iberian_biscay_irish_af = _CMEMSCommon.Dataset(
            extent_range = _CMEMSPhysicsCommon._ibi_range,
            spatial_resolution = _CMEMSPhysicsCommon._ibi_spatial_resolution,
            depth_levels = 50,

            time_resolution = dt_timedelta(days=1),

            service_id = _CMEMSPhysicsCommon._ibi_af_service_id,
            product_id = 'cmems_mod_ibi_phy_anfc_0.027deg-3D_P1D-m',
        ),
    )


class CMEMSPhysics2D(_CMEMSCommon, OpenBoundaryPhysics2D):

    datasets = dict(
        global_af = _CMEMSCommon.Dataset(
            extent_range = _CMEMSPhysicsCommon._global_range,
            spatial_resolution = _CMEMSPhysicsCommon._global_spatial_resolution,
            depth_levels = 1,

            time_resolution = dt_timedelta(hours=1),

            service_id = _CMEMSPhysicsCommon._global_af_service_id,
            product_id = 
                        'global-analysis-forecast-phy-001-024-hourly-t-u-v-ssh',
        ),
        iberian_biscay_irish_af = _CMEMSCommon.Dataset(
            extent_range = _CMEMSPhysicsCommon._ibi_range,
            spatial_resolution = _CMEMSPhysicsCommon._ibi_spatial_resolution,
            depth_levels = 0,

            time_resolution = dt_timedelta(hours=1),

            service_id = _CMEMSPhysicsCommon._ibi_af_service_id,
            product_id = 'cmems_mod_ibi_phy_anfc_0.027deg-2D_PT1H-m',
        ),
    )


class CMEMSBiogeochemical3D(_CMEMSCommon, OpenBoundaryPhysics2D):

    _ibi_af_service_id = 'IBI_ANALYSISFORECAST_BGC_005_004-TDS'

    datasets = dict(
        iberian_biscay_irish_af_hourly = _CMEMSCommon.Dataset(
            extent_range = _CMEMSPhysicsCommon._ibi_range,
            spatial_resolution = _CMEMSPhysicsCommon._ibi_spatial_resolution,

            time_resolution = dt_timedelta(hours=1),

            service_id = _ibi_af_service_id,
            product_id = 'cmems_mod_ibi_phy_anfc_0.027deg-3D_PT1H-m',
        ),
        iberian_biscay_irish_af_daily = _CMEMSCommon.Dataset(
            extent_range = _CMEMSPhysicsCommon._ibi_range,
            spatial_resolution = _CMEMSPhysicsCommon._ibi_spatial_resolution,

            time_resolution = dt_timedelta(days=1),

            service_id = _ibi_af_service_id,
            product_id = 'cmems_mod_ibi_bgc_anfc_0.027deg-3D_P1D-m',
        ),
    )



class CMEMSDatasets:

    worldwide_range = dict(
        lon = (-180., 180.),
        lat = (-90., 90.),
        depth = (0., 5500.),
    )

    iberian_biscay_irish_2d_range = dict(
        lon = (-19., 5.),
        lat = (26., 56.),
    )

    iberian_biscay_irish_3d_range = dict(
        **iberian_biscay_irish_2d_range,
        depth = (0., 5500.),
    )

    dimensional_variables_2d_map = OrderedDict(
        time = 'time',
        lat = 'latitude',
        lon = 'longitude',
    )

    dimensional_variables_3d_map = OrderedDict(
        time = 'time',
        depth = 'depth',
        lat = 'latitude',
        lon = 'longitude',
    )

    physics_variables_2d_map = dict(
        elevation = 'zos',
    )

    physics_variables_3d_map = dict(
        salinity = 'so',
        temperature = 'thetao',
    )

    biogeochemical_variables_3d_map = dict(
        chlorophyll = 'chl',
        ammonium = 'nh4',
        nitrate = 'no3',
        phosphate = 'po4',
        silicate = 'si',
        phytoplankton = 'phyc',
        dissolved_oxygen = 'o2',
    )

    # hourly_details = dict(
    #     time_offset = dt_timedelta(minutes=30),
    # )

    # daily_details = dict(
    #     time_offset = dt_timedelta(hours=12),
    # )

    common_details = dict(
        period = dt_timedelta(days=5),
        daily_updates = ( dt_time(0), ),

        subsetting = True,
    )

    global_analysis_forecast_physics_details = dict(
        **common_details,

        # motu_url = 'http://nrtcmems.mercator-ocean.fr/motu-web/Motu',

        service_id = 'GLOBAL_ANALYSIS_FORECAST_PHY_001_024-TDS',

        extent_range = worldwide_range,
        spatial_resolution = 1 / 12,

        dim_variable_map = dimensional_variables_3d_map,
    )

    iberian_biscay_irish_common_details = dict(
        **common_details,

        spatial_resolution = 1 / 36,
    )

    iberian_biscay_irish_analysis_forecast_physics_details = dict(
        **iberian_biscay_irish_common_details,

        service_id = 'IBI_ANALYSISFORECAST_PHY_005_001-TDS',
    )

    iberian_biscay_irish_analysis_forecast_biogeochemical_details = dict(
        **iberian_biscay_irish_common_details,

        service_id = 'IBI_ANALYSISFORECAST_BGC_005_004-TDS',
    )

    datasets = dict(
        global_afp_0p083_hourly = dict(
            **global_analysis_forecast_physics_details,
            #**hourly_details,

            product_id =
                        'global-analysis-forecast-phy-001-024-hourly-t-u-v-ssh',

            time_resolution = dt_timedelta(hours=1),
            time_offset = dt_timedelta(minutes=30),

            depth_levels = 1,

            variable_map = physics_variables_2d_map,
        ),
        global_afp_0p083_3d_daily = dict(
            **global_analysis_forecast_physics_details,
            #**daily_details,

            product_id = 'global-analysis-forecast-phy-001-024',

            time_resolution = dt_timedelta(days=1),
            time_offset = dt_timedelta(hours=12),

            depth_levels = 43,

            variable_map = physics_variables_3d_map,
        ),

        ibi_afp_0p028_hourly = dict(
            **iberian_biscay_irish_analysis_forecast_physics_details,
            #**hourly_details,

            product_id = 'cmems_mod_ibi_phy_anfc_0.027deg-2D_PT1H-m',

            time_resolution = dt_timedelta(hours=1),
            time_offset = dt_timedelta(minutes=30),

            extent_range = iberian_biscay_irish_2d_range,
            depth_levels = 0,

            variable_map = physics_variables_2d_map,
            dim_variable_map = dimensional_variables_2d_map,
        ),
        ibi_afp_0p028_3d_daily = dict(
            **iberian_biscay_irish_analysis_forecast_physics_details,
            #**daily_details,

            product_id = 'cmems_mod_ibi_phy_anfc_0.027deg-3D_P1D-m',

            time_resolution = dt_timedelta(days=1),
            time_offset = dt_timedelta(hours=12),

            extent_range = iberian_biscay_irish_3d_range,
            depth_levels = 50,

            variable_map = physics_variables_3d_map,
            dim_variable_map = dimensional_variables_3d_map,
        ),
        ibi_afp_0p028_3d_hourly = dict(
            **iberian_biscay_irish_analysis_forecast_physics_details,
            #**hourly_details,

            product_id = 'cmems_mod_ibi_phy_anfc_0.027deg-3D_PT1H-m',

            time_resolution = dt_timedelta(hours=1),
            time_offset = dt_timedelta(minutes=30),

            extent_range = iberian_biscay_irish_3d_range,
            depth_levels = 50,

            variable_map = physics_variables_3d_map,
            dim_variable_map = dimensional_variables_3d_map,
        ),

        ibi_afb_0p028_3d_daily = dict(
            **iberian_biscay_irish_analysis_forecast_biogeochemical_details,
            #**daily_details,

            product_id = 'cmems_mod_ibi_bgc_anfc_0.027deg-3D_P1D-m',

            time_resolution = dt_timedelta(days=1),
            time_offset = dt_timedelta(hours=12),

            extent_range = iberian_biscay_irish_3d_range,
            depth_levels = 50,

            variable_map = biogeochemical_variables_3d_map,
            dim_variable_map = dimensional_variables_3d_map,
        ),
    )


class CMEMS(CMEMSDatasets, Provider, OpenBoundary):

    logger = logger.getChild(__qualname__)

    default_elev_offset_nc = 'GLO-MFC_001_024_mdt.nc'

    def __init__(self, *, motuclient, motu_url=None, config_file=None,
                                                                      **kwargs):

        self.motuclient_path = Path(motuclient)
        self.motu_url = motu_url
        self.config_file = config_file

        super().__init__(**kwargs)

    @classmethod
    def download_product(cls, *, motuclient, variables, **motu_kwargs):

        cls.logger.info('motuclient: %s, variables: %s, motu_args: %s',
                                             motuclient, variables, motu_kwargs)

        flat_args = lambda args: tuple( chain.from_iterable(args) )

        date_fmt = '%Y-%m-%d %H:%M:%S'
        is_date = lambda arg: isinstance(arg, dt_date) or \
                                                    isinstance(arg, dt_datetime)
        arg = lambda kwarg: '--{}'.format( kwarg.replace('_', '-') )
        str_ = lambda value: value.strftime(date_fmt) if is_date(value) \
                                                                 else str(value)
        motu_args = flat_args( ( arg(kwarg), str_(value) )
                                       for kwarg, value in motu_kwargs.items() )

        variables_args = flat_args( ('--variable', var) for var in variables )

        print(motuclient, *motu_args, *variables_args)
        motu_exec = cls._run_bin(motuclient, *motu_args, *variables_args)

        return motu_exec

    default__get_dataset_file_path = Path('/tmp/cmems.nc')

    def _get_dataset(self, date, variables=None, file_path=None,
                                                              delete_file=True):
        self.logger.info('_get_dataset: date=%s', date)

        file_path_ = file_path or self.default__get_dataset_file_path
        (lat_min, lat_max), (lon_min, lon_max) = map( self.extent.__getitem__,
                                                                ('lat', 'lon') )

        ds = self.dataset
        time_offset = ds['time_offset']
        spatial_offset = ds['spatial_resolution']
        date_time = dt_datetime.combine( date, dt_time(0) )

        motuclient_kwargs = dict(
            service_id = ds['service_id'],
            product_id = ds['product_id'],
            date_min = date_time - time_offset,
            date_max = date_time + self.period + time_offset,
            latitude_min = lat_min - spatial_offset,
            latitude_max = lat_max + spatial_offset,
            longitude_min = lon_min - spatial_offset,
            longitude_max = lon_max + spatial_offset,
            out_dir = file_path_.parent,
            out_name = file_path_.name,
        )

        ds_depth_range = ds['extent_range'].get('depth')
        if ds_depth_range:
            depth_min, depth_max = self.extent.get('depth') or ds_depth_range
            motuclient_kwargs.update(
                depth_min = depth_min,
                depth_max = depth_max,
            )

        motu_url = self.motu_url or ds.get('motu_url')
        if motu_url:
            motuclient_kwargs['motu'] = motu_url
        
        if self.config_file:
            motuclient_kwargs['config-file'] = self.config_file

        try:
            self.download_product(
                motuclient = str(self.motuclient_path),
                variables = variables or ds['variable_map'].values(),

                **motuclient_kwargs,
            )
            nc_dataset = Dataset(file_path_)

        finally:
            if delete_file:
                self.logger.info('unlink: %s', file_path_)
                file_path_.unlink()

        return nc_dataset


def main(dataset, year, month, day, period_hours, x_min, x_max, y_min, y_max):

    boundaries = (
        dict(
            kind = 'ocean',
            node_count = 34,
        ),
        dict(
            kind = 'river',
            flux_value = -15.,
            node_count = 2,
        ),
        dict(
            kind = 'river',
            flux_value = -300.,
            node_count = 6,
        ),
    )

    forcing = lambda ds_key: Motu(
        dataset_key = ds_key,
        update = dt_time(0),
        period = dt_timedelta(hours=48),
        extent = dict(
            lon = ( float(x_min), float(x_max) ),
            lat = ( float(y_min), float(y_max) ),
        ),

        boundaries = boundaries,
        depth_levels = 39,
        timestep = dt_timedelta(seconds=48),

        # utils_dir = Path(),
        motuclient = Path('/home/jrogeiro/projects/opencoasts/'
                                           'py362_venv-ipython/bin/motuclient'),

        utils_dir = 'utils',
        templates_dir = 'templates',
    )

    dataset_map = dict(
        global_afp_0p083 = dict(
            elev = 'global_afp_0p083_hourly',
            salt_temp = 'global_afp_0p083_3d_daily',
        ),
        ibi_afp_0p028 = dict(
            elev = 'ibi_afp_0p028_hourly',
            salt_temp = 'ibi_afp_0p028_3d_daily',
        ),
        ibi_afb_0p028 = dict(
            eco = 'ibi_afb_0p028_3d_daily',
        )
    )

    # forcing_elev = forcing(dataset_map[dataset]['elev'])
    # forcing_salt_temp = forcing(dataset_map[dataset]['salt_temp'])

    date = dt_date( *map( int, (year, month, day) ) )

    forcings_bio = forcing(dataset_map[dataset]['eco'])

    forcings_bio.create_ECO_3D_th(date=date)

    # forcing_elev.create_elev2D_th(date=date)
    # forcing_salt_temp.create_TEM_3D_th(date=date)
    # forcing_salt_temp.create_SAL_3D_th(date=date)

    # forcing_elev.close()
    # forcing_salt_temp.close()


if __name__ == '__main__':

    logger_lvl = 'DEBUG' if DEBUG else 'INFO'
    logger_fmt = '%(asctime)s | %(name)s | %(message)s'
    basicConfig(level=logger_lvl, format=logger_fmt)

    main(*argv[1:])
