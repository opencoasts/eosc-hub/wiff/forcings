import netCDF4
import requests
import sys
from datetime import datetime, time, timedelta
from tempfile import NamedTemporaryFile
from xml.etree import ElementTree as xml_etree

from .. import Provider
from ...targets.sflux import SfluxAir, SfluxRad


class Mandeo(Provider, SfluxAir, SfluxRad):


    class Dataset(Provider.Dataset):

        # variable_map = dict
        path_template = str

    # common_variable_map = dict(
    #     wind10_u = 'u',
    #     wind10_v = 'v',
    #     presure0 = 'mslp',
    #     temp2 = 'temp',
    #     rh2 = 'rh',
    #     dlwrf0 = 'lwflx',
    #     dswrf0 = 'swflx',
    # )

    datasets = dict(
        west_europe = Dataset(
            extent_range = dict(
                lon = (-49.183, 18.789),
                lat = (24.038, 56.067),
            ),
            grid_size = dict(
                horizontal = 118, # 0.58
                vertical = 104, # 0.31
            ),

            period = timedelta(hours=84),
            daily_updates = Dataset._every_twelve_hours,
            time_resolution = timedelta(hours=1),
            # variable_map = common_variable_map,

            path_template = 'wrf_2d_36km/fmrc/files/{date:%Y%m%d}' \
                     '/wrf_arw_det_history_d01_{date:%Y%m%d}_{update:%H}00.nc4',
        ),
        iberia_biscay = Dataset(
            extent_range = dict(
                lon = (-21.579, 6.358),
                lat = (33.636, 49.568),
            ),
            grid_size = dict(
                horizontal = 171, # 0.17
                vertical = 138, # 0.12
            ),

            period = timedelta(hours=84),
            daily_updates = Dataset._every_twelve_hours,
            time_resolution = timedelta(hours=1),
            # variable_map = common_variable_map,

            path_template = 'wrf_2d_12km/fmrc/files/{date:%Y%m%d}' \
                     '/wrf_arw_det_history_d02_{date:%Y%m%d}_{update:%H}00.nc4',
        ),
        galicia = Dataset(
            extent_range = dict(
                lon = (-11.261, -5.102),
                lat = (40.421, 45.230),
            ),
            grid_size = dict(
                horizontal = 117, # 0.053
                vertical = 126, # 0.038
            ),

            period = timedelta(hours=84),
            daily_updates = Dataset._every_twelve_hours,
            time_resolution = timedelta(hours=1),
            # variable_map = common_variable_map,

            path_template = 'wrf_2d_04km/fmrc/files/{date:%Y%m%d}' \
                     '/wrf_arw_det_history_d03_{date:%Y%m%d}_{update:%H}00.nc4',
        ),
    )

    _base_url = 'http://mandeo.meteogalicia.es/thredds/ncss'
    _query_url = '&'.join( (
        # 'var={variables}',
        'north={north}',
        'west={west}',
        'east={east}',
        'south={south}',
        'time_start={start:%Y-%m-%dT%H:%M:%S}Z',
        'time_duration=PT{period}S',
        'accept=netcdf',
    ) )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._date, self._update = self._find_latest_dataset()
        self._dataset_url = self._dataset_url(self._date, self._update)
        self._base_query_url = self._format_base_query_url(self._dataset_url)
        self.logger.debug("dataset: %s %s at %s",
                                    self._date, self._update, self._dataset_url)

        self.__variables = dict()

    def _find_latest_dataset(self):
        last_valid_date_time = \
                              self.date_time + self.period - self.dataset.period
        #self.logger.debug("last_valid_date_time=%s", last_valid_date_time)

        def potential_date_time():
            date = self.date_time.date()
            while True:
                for update in reversed( self.dataset.daily_updates ):
                    yield date, update

                date -= timedelta(days=1)

        for date, update in potential_date_time():
            date_time = datetime.combine(date, update)
            #self.logger.debug("date=%s update=%s", date, update)

            if date_time > self.date_time:
                continue

            if date_time < last_valid_date_time:
                break

            if self._enough_dataset_period(date, update):
                return date, update

    def _enough_dataset_period(self, date, update):
        url = self._dataset_url(date, update)
        #self.logger.debug("url=%s", url)
        xml_request = requests.get(f'{url}/dataset.xml')
        #self.logger.debug("xml_request=%s", xml_request)
        
        if xml_request.status_code != requests.codes.ok:
            return False

        xml_root = xml_etree.fromstring(xml_request.text)
        xml_time_span_ = lambda tag: datetime.strptime(
                    xml_root.find(f'TimeSpan/{tag}').text, '%Y-%m-%dT%H:%M:%SZ')

        #return xml_time_span_('begin') <= self.date_time and \
        return (self.date_time+self.period) <= xml_time_span_('end')

    def _dataset_url(self, date, update):
        return '{base}/{path}'.format(
            base = self._base_url,
            path = self.dataset.path_template.format(date=date, update=update),
        )

    def _get_netcdf_with(self, variables):
        variable_str = ','.join(variables)
        variables_url = f'{self._base_query_url}&var={variable_str}'

        self.logger.debug('downloading %s at %s', variable_str, variables_url)
        request = requests.get(variables_url, stream=True)

        nc_fd = self._manage( NamedTemporaryFile(dir=self.work_path) )
        for chunk in request.iter_content(chunk_size=4096):
            nc_fd.write(chunk)

        self.logger.info('downloaded %s to %s', variables_url, nc_fd.name)

        return self._manage( netCDF4.Dataset(nc_fd.name) )

    def _format_base_query_url(self, dataset_url):
        (west, east), (south, north) = ( self.extent[axis]
                                                    for axis in ('lon', 'lat') )

        sigma_hrz = self.dataset.spatial_resolution['horizontal']
        sigma_vrt = self.dataset.spatial_resolution['vertical']

        return f'{dataset_url}?{{query}}'.format(
            query = self._query_url.format(
                north = north + sigma_vrt,
                west = west + sigma_hrz,
                east = east + sigma_hrz,
                south = south + sigma_vrt,
                start = self.date_time,
                period = self.period.total_seconds(),
            )
        )

    def _load_variables(self, names):
        variables_to_download = names - self.__variables.keys()
        netcdf_dataset = self._get_netcdf_with(variables_to_download)

        # time is present when requested with a variable with such dimension
        self.__variables.setdefault('time', netcdf_dataset.variables['time'])
        self.__variables.update( (name, netcdf_dataset.variables[name])
                                                             for name in names )

    def _variable_data(self, name):
        try:
            return self.__variables[name][:]
        except KeyError:
            raise RuntimeError(f"variable '{name}' must be already loaded!")

    def _sflux_work_path(self):
        return self.work_path

    def _sflux_date_time(self):
        return self.date_time

    def _sflux_map_data(self, *, t=None, **variable_map):
        # time is always present, it shall not be asked but must included too
        time_map = { 't': self._variable_data(t) } if t else {}
        return dict(time_map,
            **{ sflux_name: self._variable_data(netcdf_name)[:]
                           for sflux_name, netcdf_name in variable_map.items() }
        )

    @Provider.cached_method
    def _sflux_dimensional_data(self):
        return self._sflux_map_data(t='time', latt='lat', long='lon')

    @Provider.cached_method
    def _sflux_wind_data(self):
        return self._sflux_map_data(u='u', v='v')

    @Provider.cached_method
    def _sflux_air_data(self):
        return dict(self._sflux_wind_data(),
                **self._sflux_map_data(p='mslp', rh='rh', tmp='temp') )

    @Provider.cached_method
    def _sflux_rad_data(self):
        return self._sflux_map_data(dl='lwflx', ds='swflx')

    def create_sflux_air(self, *args, **kwargs):
        self._load_variables( 'lat lon u v mslp rh temp'.split() )
        super().create_sflux_air(*args, **kwargs)

    def create_sflux_rad(self, *args, **kwargs):
        self._load_variables( 'lat lon lwflx swflx'.split() )
        super().create_sflux_rad(*args, **kwargs)

    def create_sflux_wind(self, *args, **kwargs):
        self._load_variables( 'lat lon u v'.split() )
        super().create_sflux_wind(*args, **kwargs)


def download(dataset, target, kind, year, month, day, hour, period, \
                                               x_min, x_max, y_min, y_max, dir):
    from ...core import enable_logging
    enable_logging()

    mandeo = Mandeo(dataset,
        date_time = datetime( *map(int, (year, month, day, hour) ) ),
        period = timedelta( hours=int(period) ),
        extent = dict(
            lon = ( float(x_min), float(x_max) ),
            lat = ( float(y_min), float(y_max) ),
        ),
        work_path = dir,
    )

    if target == 'sflux':
        kind_list = kind.split(',')
        sflux_kwargs = dict(
            dir_path = dir,
        )

        if 'air' in kind_list:
            mandeo.create_sflux_air(**sflux_kwargs)
        if 'rad' in kind_list:
            mandeo.create_sflux_rad(**sflux_kwargs)
        if 'input' in kind_list:
            mandeo.create_inputs_txt(**sflux_kwargs)
        if 'wind' in kind_list:
            mandeo.create_sflux_wind(**sflux_kwargs)

    mandeo.close()


if __name__ == '__main__':
    from sys import argv
    download(*argv[1:])
