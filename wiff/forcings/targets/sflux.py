from abc import abstractmethod
from collections import namedtuple
from pathlib import Path

from . import Target
from ..libsflux import write_sfluxInputsTxt, write_sfluxair, write_sfluxrad


class _CommonSflux(Target):

    @abstractmethod
    def _sflux_date_time(self):
        pass

    @abstractmethod
    def _sflux_work_path(self):
        pass

    @abstractmethod
    def _sflux_dimensional_data(self):
        pass

    def create_inputs_txt(self, dir_path=None):
        dir_path = dir_path or self._sflux_work_path()
        write_sfluxInputsTxt(self._sflux_date_time(), dir_path)

        self.logger.info('%s saved!', Path(dir_path)/'sflux_inputs.txt')

    # @abstractmethod
    # def _sflux_time_data(self):
    #     pass

    # @abstractmethod
    # def _sflux_longitude_data(self):
    #     pass

    # @abstractmethod
    # def _sflux_latitude_data(self):
    #     pass


class SfluxWind(_CommonSflux):

    @abstractmethod
    def _sflux_wind_data(self):
        pass

    def create_sflux_wind(self, dir_path=None, *, name='wind.nc', zlib=True):
        dir_path = dir_path or self._sflux_work_path()
        file_path = Path(dir_path) / name

        write_sfluxair(
            file = file_path,
            date_time = self._sflux_date_time(),
            **self._sflux_dimensional_data(),
            **self._sflux_wind_data(),
            fill_value=-999.,
            zlib = zlib,
        )

        self.logger.info('%s saved!', file_path)


from functools import partial


# should it be based on SfluxWind?
class SfluxAir(_CommonSflux):

    @abstractmethod
    def _sflux_air_data(self):
        pass

    def create_sflux_air(self, dir_path=None, *, name='sflux_air_1.001.nc',
                                                                     zlib=True):
        dir_path = dir_path or self._sflux_work_path()
        file_path = Path(dir_path) / name

        write_sfluxair(
            file = file_path,
            date_time = self._sflux_date_time(),
            **self._sflux_dimensional_data(),
            **self._sflux_air_data(),
            zlib = zlib,
        )

        self.logger.info('%s saved!', file_path)

    # @abstractmethod
    # def _sflux_wind_u_data(self):
    #     pass

    # @abstractmethod
    # def _sflux_wind_v_data(self):
    #     pass

    # @abstractmethod
    # def _sflux_pressure_data(self):
    #     pass

    # @abstractmethod
    # def _sflux_specific_humidity_data(self):
    #     pass

    # def _sflux_relative_humidity_data(self):
    #     raise NotImplementedError

    # @abstractmethod
    # def _sflux_temperature_data(self):
    #     pass

    # @abstractmethod
    # def _sflux_radiation_short_data(self):
    #     pass

    # @abstractmethod
    # def _sflux_radiation_long_data(self):
    #     pass

    # def create_sflux_air(self, dir_path=None, *, name='sflux_air_1.001.nc',
    #                                                                  zlib=True):
    #     dir_path = dir_path or self._sflux_work_path()
    #     file_path = Path(dir_path) / name

    #     partial_write_sfluxair = partial(write_sfluxair,
    #         file = file_path,
    #         date_time = self._sflux_date_time(),

    #         t = self._sflux_time_data(),
    #         latt = self._sflux_latitude_data(),
    #         long = self._sflux_longitude_data(),

    #         u = self._sflux_wind_u_data(),
    #         v = self._sflux_wind_v_data(),
    #         p = self._sflux_pressure_data(),
    #         tmp = self._sflux_temperature_data(),

    #         zlib = zlib,
    #         #complevel = 4,
    #     )

    #     try:
    #         partial_write_sfluxair( sh=self._sflux_specific_humidity_data() )
    #     except NotImplementedError:
    #         partial_write_sfluxair( rh=self._sflux_relative_humidity_data() )

    #     self.logger.info('%s saved!', file_path)


class SfluxRad(_CommonSflux):

    @abstractmethod
    def _sflux_rad_data(self):
        pass

    def create_sflux_rad(self, name='sflux_rad_1.001.nc', dir_path=None, *,
                                                                     zlib=True):
        dir_path = dir_path or self._sflux_work_path()
        file_path = Path(dir_path) / name

        write_sfluxrad(
            file = file_path,
            date_time = self._sflux_date_time(),
            **self._sflux_dimensional_data(),
            **self._sflux_rad_data(),
            zlib = zlib,
        )

        self.logger.info('%s saved!', file_path)

    # @abstractmethod
    # def _sflux_radiation_long_data(self):
    #     pass

    # @abstractmethod
    # def _sflux_radiation_short_data(self):
    #     pass

    # def create_sflux_rad(self, name='sflux_rad_1.001.nc', dir_path=None, *,
    #                                                                  zlib=True):
    #     dir_path = dir_path or self._sflux_work_path()
    #     file_path = Path(dir_path) / name

    #     write_sfluxrad(
    #         file = file_path,
    #         date_time = self._sflux_date_time(),

    #         t = self._sflux_time_data(),
    #         latt = self._sflux_latitude_data(),
    #         long = self._sflux_longitude_data(),

    #         dl = self._sflux_radiation_long_data(),
    #         ds = self._sflux_radiation_short_data(),

    #         zlib = zlib,
    #         #complevel = 4,
    #     )
    #     self.logger.info('%s saved!', file_path)
