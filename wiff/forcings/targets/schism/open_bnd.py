import operator
import netCDF4
from datetime import timedelta
from enum import IntEnum
from functools import lru_cache, partial
from logging import getLogger
from pathlib import Path
from scipy.interpolate import RegularGridInterpolator
from subprocess import run, PIPE, STDOUT

logger = getLogger(__name__)


class OpenBoundary:


    class Kind:

        Ocean = 'ocean'
        River = 'river'


    class UtilBin:

        gen_fg = 'gen_fg'
        gen_bc = 'genBC_CMEMS'
        timeint_th = 'timeint_th'
        timeint_3Dth = 'timeint_3Dth2'
        vint_3Dth = 'vint_3Dth'

        gen_bc_bio = 'genBC_CMEMS_BIO'
        tracer_3Dth_bio = 'tracer_3Dth_BIO'


    class TemplateFile:

        gen_fg_in = 'gen_fg.in.template'
        gen_bc_in = 'genBC_CMEMS.in.template'
        timeint_in = 'timeint.in.template'
        vint_in = 'vint.in.template'


    class GenBCVariableKindFlag(IntEnum):

        Elevation = 1
        Salinity = 2
        Temperature = 3

    class GenBCBioKindFlag(IntEnum):

        Climatology = 1
        MonthlyHindcast = 2
        DailyHindcastOrForecast = 3


    class VintDataTypeFlag(IntEnum):

        Scalar = 1
        Vector = 2


    lru_cache_size = 8

    logger = logger.getChild(__qualname__)

    default_elev_offset_nc = 'elev_offset.nc'

    # TODO: set constant files, like fg.bp or even vgrid.in to get the
    #       depth_levels, as __ini__ args; eg: elev_offset_nc

    def __init__(self, *, boundaries, timestep, depth_levels=1, utils_dir='.',
                              templates_dir='.', elev_offset_nc=None, **kwargs):

        self.boundaries = boundaries
        self.timestep = timestep
        self.depth_levels = depth_levels

        self.elev_offset_nc = \
                   Path(elev_offset_nc or self.default_elev_offset_nc).resolve()

        self.utils_path = Path(utils_dir).resolve()
        self.templates_path = Path(templates_dir).resolve()

        super().__init__(**kwargs)

    # TODO: __repr__ & __str__ and call one of them on __init__ as a debug msg

    @classmethod
    def _run_bin(cls, bin_path, *args, **kwargs):

        run_kwargs = dict(check=True, stdout=PIPE, stderr=STDOUT,
                                              encoding='utf8', errors='replace')
        run_kwargs.update(kwargs)

        args_str = tuple( map(str, args) )
        cls.logger.info('%r args=%s kwargs=%s', bin_path, args_str, run_kwargs)

        run_args = (bin_path, *args_str)
        run_exec = run(run_args, **run_kwargs)
        cls.logger.info('%r stdout:\n%s', bin_path, run_exec.stdout)

        return run_exec

    def _util_path(self, util_bin):

        return self.utils_path / util_bin

    def _run_bin_util(self, util_bin, *args, **kwargs):

        bin_path = self._util_path(util_bin)
        return self._run_bin(bin_path, *args, **kwargs)

    @classmethod
    def _move_file(cls, path, target):

        path.replace(target)
        cls.logger.debug('%r moved to %r', path, target)

        return target

    @classmethod
    def _suffixed_file_path(cls, dir_path, file_name, suffix):

        file_path = dir_path / file_name

        if suffix:
            # file_path_suffixed = dir_path / f'{file_name}{suffix}'
            file_path_suffixed = Path(f'{file_path}{suffix}')

            #if file_path.is_symlink():
            if file_path.is_file():
                file_path.unlink()
            file_path.symlink_to(file_path_suffixed.name)

            file_path = file_path_suffixed
            cls.logger.debug('%r suffixed with %r', file_path, suffix)

        return file_path

    def _template_path(self, template_file):
        return self.templates_path / template_file

    @classmethod
    #@lru_cache(maxsize=lru_cache_size)
    def _render_template(cls, template_path, output_path, format_function,
                                                 *fmt_fn_args, **ftm_fn_kwargs):

        formatted_template = format_function(template_path.read_text(),
                                                  *fmt_fn_args, **ftm_fn_kwargs)

        output_path.write_text(formatted_template)

        cls.logger.debug(
            '%r formatted from %r (%s args=%s kwargs=%s)', output_path,
            template_path, format_function, fmt_fn_args, ftm_fn_kwargs
        )

    @lru_cache(maxsize=lru_cache_size)
    def _render_template_file(self, template_file, output_path, format_function,
                                                 *fmt_fn_args, **ftm_fn_kwargs):

        template_path = self._template_path(template_file)
        self._render_template(template_path, output_path, format_function,
                                                  *fmt_fn_args, **ftm_fn_kwargs)

    def _format_gen_fg_in(self, template):

        ocean_boundaries_segment_ids = tuple( str(segment_id)
            for segment_id, boundary in enumerate(self.boundaries, start=1)
                                        if boundary['kind'] == self.Kind.Ocean )

        return template.format(
            BOUNDARY_COUNT = len(ocean_boundaries_segment_ids),
            BOUNDARY_SEGMENT_IDS = '\n'.join(ocean_boundaries_segment_ids),
        )

    _fg_bp_file = 'fg.bp'

    @lru_cache(maxsize=lru_cache_size)
    def _run_gen_fg(self, dir_path):

        gen_fg_in_path = dir_path / 'gen_fg.in'
        self._render_template_file(self.TemplateFile.gen_fg_in, gen_fg_in_path,
                                                         self._format_gen_fg_in)

        return self._run_bin_util(self.UtilBin.gen_fg, cwd=dir_path)

    def _format_gen_bc_in(self, template, netcdf_file, variable_kind_flag,
                                 msl_offset_file, th_out_file, fg_bp_file=None):

        return template.format(
            BOUNDARY_BUILD_POINTS_FILE = fg_bp_file or self._fg_bp_file,
            FORCINGS_NETCDF_FILE = netcdf_file,
            VARIABLE_KIND_FLAG = variable_kind_flag,
            VARIABLE_DIMENSIONS = len(self.dataset['dim_variable_map']),
            MEAN_SEA_LEVEL_OFFSET_FILE = msl_offset_file,
            TIME_OFFSET_SECONDS = self.dataset['time_offset'].total_seconds(),
            OUTPUT_FILE = th_out_file,
        )

    def _run_gen_bc(self, gen_bc_in_path, nc_path, var_kind_flag,
                                                      msl_offset_path, th_path):

        self._render_template_file(
            self.TemplateFile.gen_bc_in, gen_bc_in_path, self._format_gen_bc_in,
            nc_path.name, var_kind_flag, msl_offset_path.name, th_path.name
        )

        return self._run_bin_util(self.UtilBin.gen_bc, gen_bc_in_path.name,
                                                      cwd=gen_bc_in_path.parent)

    def _format_timeint_in(self, template, depth_levels):

        ocean_boundaries_sizes = tuple(
            str(boundary['node_count']) for boundary in self.boundaries
                                          if boundary['kind'] == self.Kind.Ocean
        )

        return template.format(
            PERIOD_DAYS = self.period / timedelta(days=1),
            DEPTH_LEVELS = depth_levels,
            BOUNDARY_COUNT = len(ocean_boundaries_sizes),
            BOUNDARY_SIZES = ' '.join(ocean_boundaries_sizes),
            TIMESTEP_SECONDS = self.timestep.total_seconds(),
        )

    @lru_cache(maxsize=lru_cache_size)
    def _run_timeint_common(self, util, dir_path, input_path, output_path,
                                                  depth_levels=1, in_suffix=''):

        timeint_in_path = self._suffixed_file_path(dir_path, 'timeint.in',
                                                                      in_suffix)

        self._render_template_file(self.TemplateFile.timeint_in,
                        timeint_in_path, self._format_timeint_in, depth_levels)

        return self._run_bin_util(util, input_path.name, output_path.name,
                                                                   cwd=dir_path)


    def _run_timeint_th(self, dir_path, input_path, output_path):

        return self._run_timeint_common(self.UtilBin.timeint_th, dir_path,
                                       input_path, output_path, in_suffix='-2D')

    def _run_timeint_3Dth(self, dir_path, input_path, output_path):

        return self._run_timeint_common(self.UtilBin.timeint_3Dth, dir_path,
                    input_path, output_path, self.depth_levels, in_suffix='-3D')

    def _format_vint_in(self, template, node_index_list, data_type_flag,
                                     extended_period=True, dry_water_level=.01):
        period_extention = 2 * self.dataset['time_offset'] if extended_period \
                                                               else timedelta(0)
        return template.format(
            PERIOD_DAYS = (self.period + period_extention) / timedelta(days=1),
            DRY_WATER_LEVEL = dry_water_level,
            DATA_TYPE_FLAG = data_type_flag,
            BOUNDARY_NODE_COUNT = len(node_index_list),
            BOUNDARY_NODE_LIST = '\n'.join(node_index_list),
        )

    _build_points_file_header_size = 2

    @classmethod
    @lru_cache(maxsize=lru_cache_size)
    def _parse_build_points_file(cls, path):

        bpt_lines = Path(path).read_text().strip().splitlines()

        bpt_header_size = cls._build_points_file_header_size
        bpt_node_count = int(bpt_lines[1])

        bpt_node_lines_slice = slice(bpt_header_size,
                                                 bpt_header_size+bpt_node_count)
        bpt_node_lines = bpt_lines[bpt_node_lines_slice]

        bpt_node_matrix = ( line.split() for line in bpt_node_lines )

        return tuple( ( int(index), float(lon), float(lat), float(depth) )
                                 for index, lon, lat, depth in bpt_node_matrix )

    # TODO: dry_water_level value from param.in's h0
    def _run_vint_3Dth(self, dir_path, input_path, output_path, fg_bp_path,
                                                    in_suffix='', **fmt_kwargs):

        bpt_lines = fg_bp_path.read_text().strip().splitlines()

        bpt_header_size = 2
        bpt_node_count = int(bpt_lines[1])
        bpt_node_indexes_slice = slice(bpt_header_size,
                                                 bpt_header_size+bpt_node_count)

        node_index_per_line = lambda line: line.split()[0].strip()
        node_index_list = tuple(
                   map(node_index_per_line, bpt_lines[bpt_node_indexes_slice]) )

        vint_in_path = timeint_in_path = self._suffixed_file_path(dir_path,
                                                           'vint.in', in_suffix)
        self._render_template_file(
            self.TemplateFile.vint_in, vint_in_path, self._format_vint_in,
            node_index_list, self.VintDataTypeFlag.Scalar, **fmt_kwargs
        )

        return self._run_bin_util(self.UtilBin.vint_3Dth, input_path.name,
                                                 output_path.name, cwd=dir_path)

    @lru_cache(maxsize=lru_cache_size)
    def _create_elevation_offset_file(self, fg_bp_path, path):

        with netCDF4.Dataset(self.elev_offset_nc) as elev_offset_nc:
            lon = elev_offset_nc.variables['longitude'].__array__()
            lat = elev_offset_nc.variables['latitude'].__array__()
            offset = elev_offset_nc.variables['mdt'].__array__()

        bpt_nodes = self._parse_build_points_file(fg_bp_path)

        offset_interpolator = RegularGridInterpolator( (lat, lon), offset,
                                                              method='nearest' )

        # for older versions (at least < 1.16) of numpy
        #elev_offsets = ( f'{float( offset_interpolator( (lat, lon) ) ):.2f}'
        elev_offsets = ( f'{offset_interpolator( (lat, lon) ):.2f}'
                                       for index, lon, lat, depth in bpt_nodes )
        path.write_text( '\n'.join( elev_offsets ) )

        self.logger.debug('%r elevation offset file created', path)

    # def _create_common(self, date, output_dir, ds_file, var_kind_flag_enum):

    #     dir_path = Path(output_dir)

    #     self._run_gen_fg(dir_path)

    #     dataset_path = dir_path / ds_file

    #     nc_dataset = self.get_dataset(date=date, file_path=dataset_path,
    #                                                           delete_file=False)

    #     elevation_offset_path = dir_path / 'elev_offset.txt'
    #     self._create_elevation_offset_file(elevation_offset_path,
    #                                                   dir_path/self._fg_bp_file)

    #     genbc_in_path = dir_path / f'genbc-{var_kind_flag_enum.name}.in'
    #     genbc_th_path = genbc_in_path.with_suffix('.th')
    #     self._run_gen_bc(genbc_in_path, dataset_path, var_kind_flag_enum.value,
    #                                        elevation_offset_path, genbc_th_path)

    #     return dir_path, genbc_th_path, nc_dataset

    # def create_elev2D_th(self, *, date, output_dir='.'):

    #     dir_path, genbc_th_path, _ = self._create_common(date, output_dir,
    #                      'bc_src-elev.nc', self.GenBCVariableKindFlag.Elevation)

    #     elev2D_th_path = dir_path / 'elev2D.th'
    #     self._run_timeint_th(dir_path, genbc_th_path, elev2D_th_path)

    #     self.logger.debug('%r created', elev2D_th_path)

    def _create_common(self, date, dataset_path, var_kind_flag_enum):

        nc_dataset = self.get_dataset(date=date, file_path=dataset_path,
                                                              delete_file=False)

        dir_path = dataset_path.parent
        self._run_gen_fg(dir_path)

        fg_bp_path = dir_path / self._fg_bp_file
        elevation_offset_path = dir_path / 'elev_offset.txt'
        self._create_elevation_offset_file(fg_bp_path, elevation_offset_path)

        genbc_in_path = dir_path / f'genbc-{var_kind_flag_enum.name}.in'
        genbc_th_path = genbc_in_path.with_suffix('.th')
        self._run_gen_bc(genbc_in_path, dataset_path, var_kind_flag_enum.value,
                                           elevation_offset_path, genbc_th_path)

        return nc_dataset, genbc_th_path

    def create_elev2D_th(self, *, date, output_dir='.'):

        dir_path = Path(output_dir)
        nc_dataset_path = dir_path / 'cmems-phys_2D.nc'
        _, genbc_th_path = self._create_common(date, nc_dataset_path,
                                           self.GenBCVariableKindFlag.Elevation)

        elev2D_th_path = dir_path / 'elev2D.th'
        self._run_timeint_th(dir_path, genbc_th_path, elev2D_th_path)

        self.logger.debug('%r created', elev2D_th_path)

    @lru_cache(maxsize=lru_cache_size)
    def _create_vgrid_in_0(self, dir_path, nc_dataset):

        depth_var_name = self.dataset['dim_variable_map']['depth']
        depth_levels = nc_dataset.variables[depth_var_name][:]
        vgrid_in_0_levels = sorted(
                                  map(partial(operator.mul, -1), depth_levels) )

        vgrid_in_0_lines = (
            str( len(vgrid_in_0_levels) ),

            *( f'{level}\t{depth:.3f}'
                    for level, depth in enumerate(vgrid_in_0_levels, start=1) ),
        )

        vgrid_in_0_path = dir_path / 'vgrid.in.0'
        vgrid_in_0_path.write_text( '\n'.join(vgrid_in_0_lines) )
        self.logger.debug('%r created', vgrid_in_0_path)

    # def _create_3D_common(self, dir_path, input_path, nc_dataset, output_path):

    #     self._create_vgrid_in_0(dir_path, nc_dataset)

    #     vint_th_path = input_path.with_suffix('.vint_th')
    #     fg_bp_path = dir_path / self._fg_bp_file
    #     self._run_vint_3Dth(dir_path, genbc_th_path, vint_th_path, fg_bp_path)

    #     self._run_timeint_3Dth(dir_path, vint_th_path, output_path)

    # def create_TEM_3D_th(self, *, date, output_dir='.'):

    #     dir_path, genbc_path, nc_dataset = self._create_common(date, output_dir,
    #               'bc_src-temp_salt.nc', self.GenBCVariableKindFlag.Temperature)

    #     temp3D_th_path = dir_path / 'TEM_3D.th'
    #     self._create_3D_common(dir_path, genbc_path, nc_dataset, temp3D_th_path)

    #     self.logger.debug('%r created', temp3D_th_path)

    # def create_SAL_3D_th(self, *, date, output_dir='.'):

    #     dir_path, genbc_path, nc_dataset = self._create_common(date, output_dir,
    #                  'bc_src-temp_salt.nc', self.GenBCVariableKindFlag.Salinity)

    #     salt3D_th_path = dir_path / 'SAL_3D.th'
    #     self._create_3D_common(dir_path, genbc_path, nc_dataset, salt3D_th_path)

    #     self.logger.debug('%r created', salt3D_th_path)

    def _create_3D_common(self, date, var_kind_flag_enum, output_path):

        dir_path = output_path.parent
        nc_dataset_path = dir_path / 'cmems-phys_3D.nc'
        nc_dataset, genbc_th_path = self._create_common(date, nc_dataset_path,
                                                             var_kind_flag_enum)

        self._create_vgrid_in_0(dir_path, nc_dataset)

        vint_th_path = genbc_th_path.with_suffix('.vint_th')
        fg_bp_path = dir_path / self._fg_bp_file
        self._run_vint_3Dth(dir_path, genbc_th_path, vint_th_path, fg_bp_path,
                                                              in_suffix='-phys')

        self._run_timeint_3Dth(dir_path, vint_th_path, output_path)
        self.logger.debug('%r created', output_path)

    def create_TEM_3D_th(self, *, date, output_dir='.'):

        TEM_3D_th_path = Path(output_dir) / 'TEM_3D.th'
        self._create_3D_common(date, self.GenBCVariableKindFlag.Temperature,
                                                                 TEM_3D_th_path)

    def create_SAL_3D_th(self, *, date, output_dir='.'):

        SAL_3D_th_path = Path(output_dir) / 'SAL_3D.th'
        self._create_3D_common(date, self.GenBCVariableKindFlag.Salinity,
                                                                 SAL_3D_th_path)

    def _run_gen_bc_bio(self, bc_kind_flag, nc_path):

        return self._run_bin_util(self.UtilBin.gen_bc_bio, bc_kind_flag.value,
                                               nc_path.name, cwd=nc_path.parent)

    _tracer_3Dth_bio_input_name = 'tracer3Dth.in'

    def _run_tracer_3Dth_bio(self, wd_path, input_name=None):
        input_name_ = input_name or self._tracer_3Dth_bio_input_name

        if input_name_ != self._tracer_3Dth_bio_input_name:
            input_path = wd_path / 'tracer3Dth.in'
            input_path.symlink_to(input_name_)

        return self._run_bin_util(self.UtilBin.tracer_3Dth_bio, cwd=wd_path)

    create_ECO_3D_th_vint_input_map = {
        'chl3D_src.th': 'chl3D.th',
        'nh43D_src.th': 'nh43D.th',
        'no33D_src.th': 'no33D.th',
        'po43D_src.th': 'po43D.th',
        'si3D_src.th': 'si3D.th',
        'phyc3D_src.th': 'phyc3D.th',
        'o23D_src.th': 'o23D.th',
    }

    def create_ECO_3D_th(self, *, date, output_dir='.'):

        wd_path = Path(output_dir)

        nc_path = wd_path / 'cmems-bio_3D.nc'
        nc_dataset = self.get_dataset(date=date, file_path=nc_path,
                                                              delete_file=False)

        self._run_gen_fg(wd_path)
        fg_bp_path = wd_path / self._fg_bp_file

        bc_forecast = self.GenBCBioKindFlag.DailyHindcastOrForecast
        self._run_gen_bc_bio(bc_forecast, nc_path)

        self._create_vgrid_in_0(wd_path, nc_dataset)

        for vint_in, vint_out in self.create_ECO_3D_th_vint_input_map.items():
            vint_in_path = wd_path / vint_in
            vint_out_path = wd_path / vint_out
            self._run_vint_3Dth(wd_path, vint_in_path, vint_out_path,
                            fg_bp_path, in_suffix='-bio', extended_period=False)

        self._run_tracer_3Dth_bio(wd_path)
