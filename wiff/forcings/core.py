import os
from logging import basicConfig, DEBUG, getLogger

def enable_logging(level=None, format='%(asctime)s :: %(name)s | %(message)s'):
    level = level or os.getenv('WIFF_LOG_LEVEL') or DEBUG
    basicConfig(level=level, format=format)

class Logging:

    logger = getLogger(__package__)

    def __init__(self, *, logger_root=None, logger_name=None, **kwargs):
        logger_root = logger_root or type(self).logger

        self.logger = logger_root.getChild(type(self).__qualname__)
        if logger_name:
            self.logger = self.logger.getChild(logger_name)

        super().__init__(**kwargs)


class WorkPlace:
    pass


class Manager:
    pass
